﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    void Start()
    {
    }
    public static void BeingAttacked(GameObject obj,Color hitColorStart, Color hitColorEnd)
    {
        obj.GetComponent<Renderer>().material.color = Color.Lerp(hitColorStart,hitColorEnd,0.5f);
    }
    void Update()
    {
        
    }
    public static void Die(GameObject obj)
    {
        Destroy(obj);
    }
}
