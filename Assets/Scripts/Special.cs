﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Special : Enemy<float>
{
    public override void ReceiveDamage(float damage)
    {
        base.ReceiveDamage(damage);
        health-=damage;
        if (health<=0)
        {
            Die();
        }
    }
}
