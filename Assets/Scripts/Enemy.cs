﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemy<T> : MonoBehaviour, IDamagable<T>
{
    public T baseHealth;
    public T currentHealth;
    public Color hitColor;
    public T health
    {
        get
        {
            return currentHealth;
        }
        set
        {
            currentHealth = value;
        }
    }
    void Start()
    {
        health=baseHealth;
    }
    public virtual void ReceiveDamage(T damage)
    {
        Color tmpColor = GetComponent<Renderer>().material.color;
        UIManager.BeingAttacked(gameObject,tmpColor,hitColor);
    }
    public void Die()
    {
        UIManager.Die(gameObject);
    }
}
