﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAttackable<T>
{
    T damage {get; set;}
    void Attack(T damage);
}
