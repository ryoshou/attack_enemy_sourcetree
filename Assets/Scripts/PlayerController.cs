﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour, IAttackable<float>
{
    public float speed;
    public float baseDamage;
    public float currentDamage;
    public float damage
    {
        get
        {
            return currentDamage;
        }
        set
        {
            currentDamage = value;
        }
    }
    private Rigidbody rb;
    public void Attack(float damage)
    {
        
    }
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        damage = baseDamage;
    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            collision.gameObject.GetComponent<Enemy<float>>().ReceiveDamage(damage);
        }
    }
    void Update()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(moveHorizontal,0.0f,moveVertical);
        rb.AddForce(movement * speed);
    }
}
