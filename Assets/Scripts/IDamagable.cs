﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamagable<T>
{
    T health {get; set;}
    void ReceiveDamage(T damage);
}
